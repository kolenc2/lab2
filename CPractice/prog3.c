#include <stdio.h>

int main() {
	//ANSWERS: (1,B), (2,A), (3,D), (4,B)
	char ans1 = 'e';
	char ans2 = 'e';
	char ans3 = 'e';
	char ans4 = 'e';
	int correct = 0;

	printf("1. Which of the following accesses a variable in structure b?\n\tA. b->var;\n\tB. b.var;\n\tC. b-var;\n\tD. b>var;\n");
	printf("Enter your answer to question 1 as a char: ");
	scanf(" %c", &ans1);
	
	printf("\n2. Which of the following accesses a variable in a pointer to a structure b*?\n\tA. b->var;\n\tB. b.var;\n\tC. b-var;\n\tD. b>var;\n");
	printf("Enter your answer to question 2 as a char: ");
	scanf(" %c", &ans2);

	printf("\n3. Which of the following is a properly defined struct?\n\tA. struct {int a;}\n\tB. struct a_struct {int a;}\n\tC. struct a_struct int a;\n\tD. struct a_struct {int a;};\n");
	printf("Enter your answer to question 3 as a char: ");
	scanf(" %c", &ans3);

	printf("\n4. Which properly declares a variable of struct foo?\n\tA. struct foo;\n\tB. struct foo var;\n\tC. foo;\n\tD. int foo;\n");
	printf("Enter your answer to question 4 as a char: ");
	scanf(" %c", &ans4);

	if(ans1 == 'b' || ans1 == 'B')
		correct++;
	if(ans2 == 'a' || ans2 == 'A')
		correct++;
	if(ans3 == 'd' || ans3 == 'D')
		correct++;
	if(ans4 == 'b' || ans4 == 'B')
		correct++;
	printf("\nTotal number of answers correct: %i\n", correct);
	
	return 0;
}
