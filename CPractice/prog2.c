#include <stdio.h>

int main() {
	//ANSWERS: (1,D), (2,A), (3,D)
	char ans1 = 'e';
	char ans2 = 'e';
	char ans3 = 'e';
	int correct = 0;

	printf("1. Which of the following is proper delcaration of a pointer?\n\tA. int x;\n\tB. int &X;\n\tC. ptr x;\n\tD. int *x;\n");
	printf("Enter your answer to question 1 as a char: ");
	scanf(" %c", &ans1);
	
	printf("\n2. Which of the following gives the memory address of integer variable a?\n\tA. *a;\n\tB. a;\n\tC. &a;\n\tD. address(a);\n");
	printf("Enter your answer to question 2 as a char: ");
	scanf(" %c", &ans2);

	printf("\n3. Which of the following gives the the value stored at the address pointed to by pointer a?\n\tA. a;\n\tB. val(a);\n\tC. *a;\n\tD. &a;\n");
	printf("Enter your answer to question 3 as a char: ");
	scanf(" %c", &ans3);

	if(ans1 == 'd' || ans1 == 'D')
		correct++;
	if(ans2 == 'a' || ans1 == 'A')
		correct++;
	if(ans3 == 'd' || ans1 == 'D')
		correct++;
	printf("\nTotal number of answers correct: %i\n", correct);
	
	return 0;
}
