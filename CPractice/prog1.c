#include <stdio.h>

int main() {
	int toPrint = 123456;
	printf("toPrint printed as an int %i", toPrint);
	printf("\ntoPrint printed as a decimal %d", toPrint);
	printf("\ntoPrint printed as a float %f", (float)toPrint);
	printf("\ntoPrint printed in octal %o", toPrint);
	printf("\ntoPrint printed in hex %X", toPrint);
	return 0;
}
