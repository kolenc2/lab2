#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void *printHelloThread(void *threadId){
	unsigned long int *id = (unsigned long int *)threadId;
	printf("Hello World! I am thread #%lu\n", *id);
}

int main(int argc, char* argv[]){
	int x;
	pthread_t thread_id;
	x = atoi(argv[1]);
	pthread_t threadList[x];
	printf("ARG INPUT: %s\n", argv[1]);

	if(x < 1 || x > 10){
		printf("Input number must be between 1-10, exiting.... \n");
		exit(0);
	}
	for(int i = 0; i < x; i++){ 
		pthread_create(&threadList[i], NULL, printHelloThread, (void *)&threadList[i]);
		printf("Creating thread #%lu \n", (unsigned long int)threadList[i]); 
	}
	
	for(int i = 0; i < x; i++){
		pthread_join(threadList[i], NULL);
		 
	}

	pthread_exit(NULL);
	return 0;
}
