#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
int main(int argc, char* argv[]){

	int x;
	if(argc > 1){
		x = atoi(argv[1]);
		printf("Args: %s \n", argv[0]);
		printf("x is: %d \n", x);
		goto go;
	}
	else{	
		printf("No args\n");
		x = -1;
	}

	pid_t pid;
	printf("START\n");
	pid = fork();
	if(pid == 0){
		while(x <= 0){
			printf("Number must be positive \n");
			scanf("%d", &x);
		}
		go:
		while(x > 1){
			if(x%2 == 0){
				x = x/2;
			}
			else{
				x *=3;
				x+=1;
			}
			printf("%d\n", x);
		}	
	}

	else{
		wait(NULL);
		printf("END");
	}

	return 0;
}
